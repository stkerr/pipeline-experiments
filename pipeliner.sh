#!/bin/bash

export PROJECT_NAME="${CI_PROJECT_NAMESPACE}%2F${CI_PROJECT_NAME}"
echo "Inside ${PROJECT_NAME}"

# Make a scheduled pipeline
echo "Making a scheduled pipeline"
curl --request POST --header "PRIVATE-TOKEN: $BUILD_TOKEN" --form description="Build packages" --form ref="master" --form cron="0 0 1 1 *" --form cron_timezone="UTC" --form active="true" "https://gitlab.com/api/v4/projects/$PROJECT_NAME/pipeline_schedules" | tee pipeline.txt

# Get the newly created pipeline ID
export PIPELINE_ID=`jq .id < pipeline.txt`
echo
echo "Created Pipeline ${PIPELINE_ID}"

echo "Setting pipeline variables"
curl --request POST --header "PRIVATE-TOKEN: $BUILD_TOKEN" --form "key=FUZZ_TEST" --form "value=enabled" "https://gitlab.com/api/v4/projects/$PROJECT_NAME/pipeline_schedules/$PIPELINE_ID/variables"

echo "This is the scheduled pipeline"
curl --header "PRIVATE-TOKEN: $BUILD_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_NAME/pipeline_schedules/$PIPELINE_ID"

echo "Running the pipeline"
curl --request POST --header "PRIVATE-TOKEN: $BUILD_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_NAME/pipeline_schedules/$PIPELINE_ID/play"


# Sleep for a few seconds to let the pipeline kick off
echo "Sleeping"
sleep 60

# Delete the scheduled pipeline
echo "Deleting the created pipeline"
curl --request DELETE --header "PRIVATE-TOKEN: $BUILD_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_NAME/pipeline_schedules/$PIPELINE_ID"

